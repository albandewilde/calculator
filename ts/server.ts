import {
  Application,
  Context,
  Router,
  Status,
} from "https://deno.land/x/oak/mod.ts";
import Operation from "./operation.ts";
import { Operator } from "./operator.ts";

const app = new Application();
const router = new Router();

router.get("/", async (ctx: Context) => {
  if (!ctx.request.hasBody) {
    ctx.response.status = Status.BadRequest;
    ctx.response.body = "Missing body.";
    return;
  }

  const body = await ctx.request.body({ type: "json" }).value;
  ctx.response.body = new Operation(body.a, body.b, body.op as Operator)
    .Calculate();
});

app.use(router.routes());
app.use(router.allowedMethods());
app.addEventListener("listen", () => console.log("Server is ready !"));

await app.listen("0.0.0.0:8080");
