import { assertEquals } from "https://deno.land/std@0.101.0/testing/asserts.ts";
import Operation from "./operation.ts";
import { Operator } from "./operator.ts";

Deno.test("Test addition operation", () => {
  assertEquals(new Operation(10, 5, Operator.Add).Calculate(), 15);
});

Deno.test("Test substraction operation", () => {
  assertEquals(new Operation(10, 5, Operator.Sub).Calculate(), 5);
});

Deno.test("Test multiplication operation", () => {
  assertEquals(new Operation(10, 5, Operator.Mul).Calculate(), 50);
});

Deno.test("Test dividing operation", () => {
  assertEquals(new Operation(10, 5, Operator.Div).Calculate(), 2);
});
