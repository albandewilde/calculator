enum Operator {
  Add = "add",
  Sub = "sub",
  Mul = "mul",
  Div = "div",
}

function GetFunc(op: Operator): (a: number, b: number) => number {
  switch (op) {
    case Operator.Add: {
      return (a: number, b: number) => a + b;
    }
    case Operator.Sub: {
      return (a: number, b: number) => a - b;
    }
    case Operator.Mul: {
      return (a: number, b: number) => a * b;
    }
    case Operator.Div: {
      return (a: number, b: number) => a / b;
    }
  }
}

export { GetFunc, Operator };
