import { GetFunc, Operator } from "./operator.ts";

class Operation {
  a: number;
  b: number;
  op: Operator;

  constructor(a: number, b: number, op: Operator) {
    this.a = a;
    this.b = b;
    this.op = op;
  }

  public Calculate(): number {
    const f: (a: number, b: number) => number = GetFunc(this.op);
    return f(this.a, this.b);
  }
}

export default Operation;
