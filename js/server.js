import { Application, Router, Status } from "https://deno.land/x/oak/mod.ts";

const app = new Application();
const router = new Router();

router.get("/", (ctx) => {
  if (!ctx.request.hasBody) {
    ctx.response.status = Status.BadRequest;
    ctx.response.body = "Missing body.";
    return;
  }
  ctx.response.body = "We'll calculate";
});

app.use(router.routes());
app.use(router.allowedMethods());
app.addEventListener("listen", () => console.log("Server is ready !"));

await app.listen("0.0.0.0:8080");
